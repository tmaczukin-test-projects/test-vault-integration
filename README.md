# Test Vault integration

## Setup

1. Install compiled binary for Vault PoC branch; ensure it's available via your `$PATH`.

    The sources are available at https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1398

1. Register a shell Runner and prepare configuration at `~/.gitlab-runner/config.toml.with-vault`:

    ```yaml
    concurrent = 1
    check_interval = 0

    [[runners]]
      name = "shell-with-vault"
      url = "https://gitlab.com/"
      token = "__YOUR_RUNNER_TOKEN__"
      executor = "shell"
      request_concurrency = 1
      [runners.vault]
        [runners.vault.server]
          url = "https://127.0.0.1:8443"
          tls_ca_file = "/tmp/vault-service-ca.crt"
        [runners.vault.auth]
          [runners.vault.auth.token]
            token = "__VAULT_ROOT_TOKEN__"
        [[runners.vault.secrets]]
          type = "kv1"
          path = "kv1/my-secrets"
          [[runners.vault.secrets.keys]]
            key = "keyBool"
            env_name = "VAULT_ENV_KEY_1"
        [[runners.vault.secrets]]
          type = "kv2"
          path = "kv2/data/my-secrets"
          [[runners.vault.secrets.keys]]
            key = "keyInt"
            env_name = "VAULT_ENV_KEY_2"
    ```

1. Start the [`vault-ci-service`](https://gitlab.com/gitlab-org/ci-cd/tests/vault-ci-service) locally on
   Runner's machine. For this:

    - install [Vault binary](https://www.vaultproject.io/downloads.html) localy on Runner's machine; ensure it's
      available via your `$PATH`.

    - download the CI service binary (current version at https://gitlab.com/gitlab-org/ci-cd/tests/vault-ci-service/-/jobs/208136316/artifacts/file/build/vaultcs):

        ```bash
        wget https://gitlab.com/gitlab-org/ci-cd/tests/vault-ci-service/-/jobs/224804571/artifacts/raw/build/vaultcs
        ```

    - make the downloaded file executable:

        ```bash
        chmod +x vaultcs
        ```

    - start the service:

        ```bash
        ./vaultcs
        ```

1. Open a new terminal and prepare the `/tmp/vault-service-ca.crt` file:

    ```bash
    curl -sk https://127.0.0.1:8443/metadata | jq .CA.CACert.CertificatePEM --raw-output > /tmp/vault-service-ca.crt
    ```

1. Next, get the Root Token to set as vault's `token` field in `[runners.vault.auth.token]` section:

    ```bash
    curl -sk https://127.0.0.1:8443/metadata | jq .RootToken --raw-output
    ```

    Copy the outputed string and put it in `config.toml` file.

1. Start the Runner and next start the job from this project:

    ```bash
    cd /tmp
    gitlab-runner run --config ~/.gitlab-runner/config.toml.with-vault
    ```

